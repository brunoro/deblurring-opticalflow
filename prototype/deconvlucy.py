import cv
import math
import numpy as np

# -- deconvolution follows this principle:
# g = f * h
# g - f * h = 0
# f = f + g - f * h
# f_{k + 1} = f_k + (g - f_k * h)
# -- where:
# g is the convolved image
# f is the deconvolved image
# h is a convolution kernel
# w is the convergence weight factor
def deconvlucy(g, h, iters):
    # on the first iteration f = g
    corr  = cv.CreateMat(g.height, g.width, cv.CV_8UC1)
    convl = cv.CreateMat(g.height, g.width, cv.CV_8UC1)
    f = g
    #cv.NamedWindow('iter', cv.CV_WINDOW_AUTOSIZE)
    for i in range(iters):
        #cv.ShowImage('iter', f)
        #cv.WaitKey()

        # convl = f_k * h
        cv.Filter2D(f, convl, h)
        # error = g - convl
        cv.Sub(g, convl, corr)
        # f_{k + 1} = f_k + error
        cv.Add(f, corr, f)

        #print i, np.asarray(convl).mean(), np.asarray(corr).mean(), np.asarray(f).mean()
    #cv.ShowImage('iter', f)
    #cv.WaitKey()
    return f
