#!/usr/bin/env python
# coding: utf-8

import cv2.cv as cv
import deconvlucy
import math
import numpy as np
import scipy.signal as sig
import sys
import time

# size of the captured frame
FRAME_WIDTH  = 320
FRAME_HEIGHT = 240

# the size of the motion detection grid
GRID_ROWS = 2
GRID_COLS = 2

# window size used on Lucas-Kanade algorithm
LK_WINDOW_SIZE = (5, 5)

# the biggest flow vector we will consider
MAX_MOTION = 0.08 #4.0 #1.4

# the magical number that says if we're going to deconvolve
MOTION_THRESHOLD = MAX_MOTION / 10.0

# the number of pixels we're skipping when getting the mean flow
MEAN_FLOW_STEP = 10
MEAN_FLOW_STEP_2 = MEAN_FLOW_STEP ** 2

# maybe we want to display something
FONT = cv.InitFont(cv.CV_FONT_HERSHEY_SIMPLEX, 0.5, 0.5)

# number of columns used to range the windows
DISPLAY_COLS = 2

# capture a frame and load it on a 8C1 matrix
def capture_frame(capture):
    cframe = cv.QueryFrame(capture)
    frame  = cv.CreateMat(cframe.rows, cframe.cols, cv.CV_8UC1)
    cv.ConvertImage(cframe, frame)
    return frame

def smooth_image(frame):
    sframe = cv.CreateMat(frame.rows, frame.cols, cv.CV_8UC1)
    cv.Smooth(frame, sframe)
    return sframe

def calc_optical_flow(frame1, frame2):
    velx = cv.CreateMat(frame1.rows, frame1.cols, cv.CV_32FC1)
    vely = cv.CreateMat(frame1.rows, frame1.cols, cv.CV_32FC1)
    
    #cv.CalcOpticalFlowLK(frame1, frame2, LK_WINDOW_SIZE, velx, vely)
    cv.CalcOpticalFlowHS(frame1, frame2, 0, velx, vely, 
                         0.5, (cv.CV_TERMCRIT_ITER, 3, 0))
    return (velx, vely)

def estimate_motion_vector(frame, flow):
    xsum = 0.0
    xsig = 0.0
    ysum = 0.0
    ysig = 0.0
    for i in xrange(0, frame.rows, MEAN_FLOW_STEP):
        for j in xrange(0, frame.cols, MEAN_FLOW_STEP):
            xsum += flow[1][i, j]
            ysum += flow[0][i, j]
    npixels = frame.rows * frame.cols / MEAN_FLOW_STEP_2
    # normalize on [0, 1]
    motion_vector = ((xsum / npixels) / MAX_MOTION, 
                     (ysum / npixels) / MAX_MOTION)
    
    # if any of the components is bigger than the threshold
    cmp = np.abs(motion_vector) > (MOTION_THRESHOLD, MOTION_THRESHOLD)
    if cmp.any():
        return motion_vector
    else:
        return 0, 0

def psf_from_motion(motion_vector):
    if motion_vector == (0, 0):
        return None

    else:
        # get the motion vector
        motion_norm = math.sqrt(motion_vector[0] ** 2 + \
                                motion_vector[1] ** 2)
        cos_theta = motion_vector[0] / motion_norm
        angular = math.tan(math.acos(cos_theta))
    
        # size the psf according to the motion vector norm
        psf_size = 2 * int(math.floor(math.log(motion_norm / 2 * 1000)) - 2) + 1
        psf_mid = int(math.floor(psf_size / 2))
        psf = cv.CreateMat(psf_size, psf_size, cv.CV_32FC1)
    
        for i in xrange(0, psf_size):
            for j in xrange(0, psf_size):
                psf[i, j] = 0
        
        # apply x's on the grid to the line
        for i in xrange(0, psf_mid):
            rel_i = float(i) / psf_size
            rel_j = angular * rel_i
            j = int(math.floor(rel_j * psf_size))
            j = psf_mid - 1 if j >= psf_mid else j
            j = -psf_mid + 1 if j <= -psf_mid else j
            psf[i + psf_mid, j + psf_mid] = \
                math.exp(math.sqrt((psf_mid - i) ** 2 + (psf_mid - j) ** 2))

        # normalize it
        psf_norm = np.asarray(psf)
        psf_norm /= np.sum(psf_norm)
        psf = cv.fromarray(psf_norm)
        
        # flip ?
        # cv.Flip(psf, psf, -1)
        
        return psf

def deconvolve(frame, kernel, iters=3):
    return deconvlucy.deconvlucy(frame, kernel, iters=iters)
    #return cv.fromarray(sig.wiener(np.asarray(frame), (11, 11)))

def draw_flow_field(frame, flow):
    for i in xrange(0, frame.rows, 10):
        for j in xrange(0, frame.cols, 10):
            basepoint = (j, i)
            endpoint  = (int(j + flow[1][i, j]),
                         int(i + flow[0][i, j]))
            cv.Line(frame, basepoint, endpoint, cv.RGB(255, 0, 0), 1, cv.CV_AA)

def draw_motion_vector(frame, vector):
    basepoint = (int(frame.cols / 2),
                 int(frame.rows / 2))
    endpoint  = (int(basepoint[0] + vector[1] * 50),
                 int(basepoint[1] + vector[0] * 50))
    cv.Line(frame, basepoint, endpoint, cv.RGB(0, 255, 0), 2, cv.CV_AA)

def draw_psf(frame, psf):
    psf8 = cv.CreateMat(psf.rows, psf.cols, cv.CV_8UC1)
    psf8big = cv.CreateMat(frame.rows, frame.cols, cv.CV_8UC1)
    cv.ConvertScale(psf, psf8, 100)
    cv.Resize(psf8, psf8big)
    cv.Add(frame, psf8big, frame)

def color_frame(frame):
    # go to three channels again
    cframe = cv.CreateMat(frame.rows, frame.cols, cv.CV_8UC3)
    cv.ConvertImage(frame, cframe)
    return cframe

def copy_frame(frame):
    # go to three channels again
    cframe = cv.CloneMat(frame)
    return cframe

def window_pos(rowsize, colsize, initpos=(0, 0)):
    x, y = initpos
    #yoffset = 45
    y = yoffset = 50
    while True:
        yield x, y
        x += colsize 
        if x >= DISPLAY_COLS * colsize:
            x = 0
            y = y + rowsize

windows = {}
def display_frame(name, frame, pos):
    if name not in windows:
        cv.NamedWindow(name)
        windows[name] = pos
    x, y = windows[name]
    cv.ShowImage(name, frame)
    cv.MoveWindow(name, x, y)

def main(args):
    # we'll try to estimate a psf
    estimate_psf = True

    # if a filename is recieved don't use a camera
    from_cam = True
    if len(args) > 1:
        estimate_psf = False
        from_cam = False
        filename = args[1]

    if from_cam:
        # start the camera capture
        capture = cv.CreateCameraCapture(-1)
        if not capture:
            print "failed to open the capture"
            return 1

        # set its size
        cv.SetCaptureProperty(capture, cv.CV_CAP_PROP_FRAME_WIDTH, FRAME_WIDTH);
        cv.SetCaptureProperty(capture, cv.CV_CAP_PROP_FRAME_HEIGHT, FRAME_HEIGHT);
    
        # get the first frame
        prev_frame8 = cv.QueryFrame(capture)

    else:
        # get the image from a file
        prev_frame8 = cv.LoadImageM(filename, cv.CV_LOAD_IMAGE_GRAYSCALE)

    prev_frame = cv.CreateMat(prev_frame8.height, prev_frame8.width, cv.CV_8UC1)
    cv.ConvertImage(prev_frame8, prev_frame)

    # set the window position generator 
    pos = window_pos(prev_frame.rows, prev_frame.cols)

    # set the grid size 
    grid_colsize = prev_frame.cols / GRID_COLS
    grid_rowsize = prev_frame.rows / GRID_ROWS

    # uncomment those 2 lines to set the grid to the frame size
    #grid_colsize = prev_frame.cols
    #grid_rowsize = prev_frame.rows

    while True:
        if from_cam:
            # get the new frame
            frame8 = cv.QueryFrame(capture)
            frame = cv.CreateMat(prev_frame8.height, prev_frame8.width, cv.CV_8UC1)
            cv.ConvertImage(frame8, frame)

        else:
            # blur the previous one
            import blurring
            frame, psf = blurring.blur(prev_frame)

        if from_cam:
            display_frame('original', frame, pos.next())
        else:
            display_frame('original', prev_frame, pos.next())
            if not from_cam:
                cv.SaveImage('results/original.png', prev_frame)

            display_frame('blurred', frame, pos.next())
            if not from_cam:
                cv.SaveImage('results/blurred.png', frame)

        # deconvolved, motion vector, optical flow
        dframe = copy_frame(frame)
        fframe = color_frame(frame)
        mframe = color_frame(frame)
        vframe = color_frame(frame)
        wframe = color_frame(frame)
        pframe = copy_frame(frame)
        
        # smooth the frame for the optical flow
        sframe = smooth_image(dframe)

        # calculate optical flow
        flow = calc_optical_flow(prev_frame, sframe)

        # display the flow
        draw_flow_field(wframe, flow)
        display_frame('optical flow', wframe, pos.next())
        if not from_cam:
            cv.SaveImage('results/flow.png', wframe)

        for i in xrange(0, frame.rows, grid_rowsize):
            for j in xrange(0, frame.cols, grid_colsize):
                # get the window
                window = cv.GetSubRect(dframe, (j, i, grid_colsize, grid_rowsize))
                wflow = [cv.GetSubRect(flow[0], 
                                      (j, i, grid_colsize, grid_rowsize)),
                         cv.GetSubRect(flow[1], 
                                      (j, i, grid_colsize, grid_rowsize))]

                # estimate a motion blur vector
                motion_vector = estimate_motion_vector(window, wflow)

                # get a psf from that motion vector
                if estimate_psf:
                    psf = psf_from_motion(motion_vector)

                if psf != None:
                    # draw motion vector
                    wvec = cv.GetSubRect(vframe, (j, i, grid_colsize, grid_rowsize))
                    draw_motion_vector(wvec, motion_vector)

                    # draw psf
                    wpsf = cv.GetSubRect(pframe, (j, i, grid_colsize, grid_rowsize))
                    draw_psf(wpsf, psf)

                    # apply deconvolution
                    deconvolve(window, psf)
        
        # display the vectors
        display_frame('mean vectors', vframe, pos.next())
        if not from_cam:
            cv.SaveImage('results/mean.png', vframe)

        # display the psf 
        display_frame('psf', pframe, pos.next())
        if not from_cam:
            cv.SaveImage('results/psf.png', pframe)

        # iterate on the grid
        # display the deconvolved frame
        display_frame('deconvolved', dframe, pos.next())
        if not from_cam:
            cv.SaveImage('results/deconvolved.png', dframe)

        # assign the previous frame
        prev_frame = frame
        
        # show |result - original|
        diff_orig = cv.CreateMat(frame.rows, frame.cols, cv.CV_8UC1)
        cv.Sub(dframe, prev_frame, diff_orig)
        display_frame('absdiff', diff_orig, pos.next())
        if not from_cam:
            cv.SaveImage('results/diff.png', diff_orig)
        
        if from_cam:
            cv.WaitKey(1)
        else:
            cv.WaitKey()
            break

if __name__ == "__main__":
    main(sys.argv)
